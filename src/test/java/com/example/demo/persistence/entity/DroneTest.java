package com.example.demo.persistence.entity;

import com.example.demo.model.enums.DroneModel;
import com.example.demo.model.enums.DroneState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/11/22
 * Time: 10:22 PM
 */
class DroneTest {

    private Drone drone;

    @BeforeEach
    public void setup() {
        drone = new Drone();
    }

    @Test
    @DisplayName("Drone Entty Pojo OK")
    void testDroneEntityIsOk() {
        drone = new Drone(
                UUID.randomUUID().toString(),
                100,
                DroneModel.CRUISER_WEIGHT,
                DroneState.DELIVERED
        );

        assertNotNull(drone);
        assertThat(100).isEqualTo(drone.getWeight());
    }
}
