package com.example.demo.persistence.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/11/22
 * Time: 10:17 PM
 */
class MedicationTest {

    private Medication medication;

    @BeforeEach
    public void setup() {
        medication = new Medication();
    }

    @Test
    @DisplayName("Medication Entity Is OK")
    void testMedicationEntityIsOk() {
        medication = new Medication(
                1L,
                "John Doe",
                100,
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                new Timestamp(System.currentTimeMillis()),
                Drone.builder().build()
        );
        assertNotNull(medication);
        assertThat(1L).isEqualTo(medication.getId());
    }
}
