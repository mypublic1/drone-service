package com.example.demo.persistence.repository;

import com.example.demo.model.enums.DroneModel;
import com.example.demo.model.enums.DroneState;
import com.example.demo.persistence.entity.Drone;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/11/22
 * Time: 10:27 PM
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
class DroneRepositoryTest {

    @Autowired
    private DroneRepository repository;
    private Drone drone;

    @BeforeEach
    public void setup() {
        drone = new Drone(
                UUID.randomUUID().toString(),
                100,
                DroneModel.CRUISER_WEIGHT,
                DroneState.DELIVERED
        );
    }


    @Test
    @DisplayName("Save Drone Test")
    void testSaveDrone() {
        Drone save = repository.save(drone);
        Drone fetch = repository.findById(save.getId()).orElse(null);
        assertNotNull(drone);
        assertThat(DroneModel.CRUISER_WEIGHT).isEqualTo(save.getModel());
        assertThat(save).isEqualTo(fetch);
    }

    @Test
    @DisplayName("Delete Drone By Existing ID Pass")
    void testDeleteDroneByExistingIdShouldPass() {
        Drone save = repository.save(drone);
        Drone drone = repository.findById(save.getId()).orElse(null);
        repository.deleteById(drone.getId());
        drone = repository.findById(save.getId()).orElse(null);
        assertNull(drone);
    }

    @Test
    @DisplayName("Delete Drone By Non Existing ID Fail")
    void testDeleteDroneByNonExistingIdShouldFail() {
        try {
            repository.deleteById(2001L);
        } catch (EmptyResultDataAccessException e) {
            assertThat("No class com.example.demo.persistence.entity.Drone entity with id 2001 exists!").isEqualTo(e.getMessage());
        }
    }


    @Test
    @DisplayName("Find Drone By Non Existing ID Fail")
    void testFindDroneByNonExistingIdShouldFail() {
        try {
            repository.findById(2001L);
        } catch (EmptyResultDataAccessException e) {
            assertThat("No class com.example.demo.persistence.entity.Drone entity with id 2001 exists!").isEqualTo(e.getMessage());
        }
    }

    @Test
    void testFindDroneByStateShouldPass() {
        List<Drone> drones = repository.findDroneByState(DroneState.LOADING);
        assertNotNull(drones);
    }

    @Test
    void testFindDroneByBatteryLevelShouldPass() {
        Integer response = repository.findDroneByBatteryLevel(1L).orElse(null);
        assertNull(response);
    }

    @Test
    void testFindBySerialNoShouldPass() {
        Drone response = repository.findBySerialNo("318-41-4249").orElse(null);
        assertNull(response);
    }

}
