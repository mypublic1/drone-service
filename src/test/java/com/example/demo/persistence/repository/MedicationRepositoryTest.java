package com.example.demo.persistence.repository;

import com.example.demo.persistence.entity.Drone;
import com.example.demo.persistence.entity.Medication;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;

import java.sql.Timestamp;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/11/22
 * Time: 10:51 PM
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
class MedicationRepositoryTest {

    @Autowired
    private MedicationRepository repository;
    private Medication medication;

    @BeforeEach
    public void setup() {
        medication = new Medication(
                1L,
                "John Doe",
                100,
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                new Timestamp(System.currentTimeMillis()),
                Drone.builder().build()
        );
    }


    @Test
    @DisplayName("Save Medication Test Should Pass")
    void testSaveMedication() {
        Medication save = repository.save(medication);
        Medication fetch = repository.findById(save.getId()).orElse(null);
        assertNotNull(medication);
        assertThat(1L).isEqualTo(save.getId());
        assertThat(save).isEqualTo(fetch);
    }

    @Test
    @DisplayName("Delete Medication By Existing ID Pass")
    void testDeleteMedicationByExistingIdShouldPass() {
        Medication save = repository.save(medication);
        Medication medication = repository.findById(save.getId()).orElse(null);
        repository.deleteById(medication.getId());
        medication = repository.findById(1L).orElse(null);
        assertNull(medication);
    }

    @Test
    @DisplayName("Delete Medication By Non Existing ID Fail")
    void testDeleteMedicationByNonExistingIdShouldFail() {
        try {
            repository.deleteById(medication.getId());
        } catch (EmptyResultDataAccessException e) {
            assertThat("No class com.example.demo.persistence.entity.Medication entity with id 1 exists!").isEqualTo(e.getMessage());
        }
    }


    @Test
    @DisplayName("Find Medication By Non Existing ID Fail")
    void testFindMedicationByNonExistingIdShouldFail() {
        try {
            repository.findById(medication.getId());
        } catch (EmptyResultDataAccessException e) {
            assertThat("No class com.example.demo.persistence.entity.Medication entity with id 1 exists!").isEqualTo(e.getMessage());
        }
    }
}
