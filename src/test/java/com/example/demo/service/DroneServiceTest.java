package com.example.demo.service;

import com.example.demo.core.exceptions.CustomException;
import com.example.demo.core.exceptions.WeightException;
import com.example.demo.model.enums.DroneModel;
import com.example.demo.model.enums.DroneState;
import com.example.demo.model.request.DroneRequest;
import com.example.demo.model.response.DroneResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/11/22
 * Time: 11:04 PM
 */
@SpringBootTest
class DroneServiceTest {

    @Autowired
    private DroneService droneService;
    private DroneRequest request;

    @BeforeEach
    public void setup() {
        request = new DroneRequest(
                UUID.randomUUID().toString(),
                DroneModel.CRUISER_WEIGHT,
                100,
                100,
                DroneState.DELIVERED
        );
    }

    @Test
    void testRegisteringDroneShouldPass() {
        DroneResponse response = droneService.registerDrone(request);
        assertNotNull(response);
        assertThat(100).isEqualTo(response.getWeight());
    }

    @Test
    void testRegisteringDroneWithExistingSerialNoShouldFail() {
        try {
            droneService.registerDrone(request);
        } catch (CustomException e) {
            assertThat("Drone with serial number [" + request.getSerialNo() + "] already exist").isEqualTo(e.getMessage());
        }
    }

    @Test
    void testLoadDroneWithMedicationShouldPass() {
        DroneResponse response = droneService.loadDroneWithMedication(2L, Set.of(1L));
        assertNotNull(response);
    }

    @Test
    void testLoadDroneWithMedicationShouldFail() {
        try {
            droneService.loadDroneWithMedication(1L, Set.of(1L, 2L, 3L));
        } catch (WeightException e) {
            assertThat("Medication weight is higher than the drone weight").isEqualTo(e.getMessage());
        }
    }

    @Test
    void testLoadDroneWithMedicationShouldFailDueToBatteryLevelBelow25() {
        try {
            droneService.loadDroneWithMedication(1L, Set.of(1L));
        } catch (CustomException e) {
            assertThat("The drone cannot be loaded due to battery level").isEqualTo(e.getMessage());
            assertThat(HttpStatus.PRECONDITION_FAILED.value()).isEqualTo(e.getStatus().value());
        }
    }

    @Test
    void testLoadDroneWithMedicationShouldFailIfDroneIdDoesNotExist() {
        try {
            droneService.loadDroneWithMedication(201L, Set.of(1L, 2L, 3L));
        } catch (CustomException e) {
            assertThat("Drone cannot be found").isEqualTo(e.getMessage());
            assertThat(HttpStatus.NOT_FOUND.value()).isEqualTo(e.getStatus().value());
        }
    }

    @Test
    void testCheckLoadedMedicationItemForDroneShouldPass() {
        DroneResponse response = droneService.checkLoadedMedicationItemForDrone(1L);
        assertNotNull(response);
    }

    @Test
    void testCheckLoadedMedicationItemForDroneShouldFailIfDroneIdDoesNotExist() {
        try {
            droneService.checkLoadedMedicationItemForDrone(1121L);
        } catch (CustomException e) {
            assertThat("Drone info cannot be found").isEqualTo(e.getMessage());
            assertThat(HttpStatus.NOT_FOUND).isEqualTo(e.getStatus());
        }
    }

    @Test
    void testcheckAvailableDroneForLoadingShouldPass() {
        request.setState(DroneState.IDLE);
        droneService.registerDrone(request);
        List<DroneResponse> response = droneService.checkAvailableDroneForLoading(DroneState.IDLE);
        assertNotNull(response);
    }

    @Test
    void testcheckAvailableDroneForLoadingShouldFailWithWrongState() {
        try {
            droneService.checkAvailableDroneForLoading(DroneState.valueOf("WRONG"));
        } catch (IllegalArgumentException e) {
            assertThat("No enum constant com.example.demo.model.enums.DroneState.WRONG").isEqualTo(e.getMessage());
        }
    }

    @Test
    void checkDroneBatteryLevelShouldPass() {
        Integer response = droneService.checkDroneBatteryLevel(1L);
        assertNotNull(response);
        assertTrue(response > 19);
    }

    @Test
    void checkDroneBatteryLevelShouldFailIfDroneIdDoesNotExist() {
        try {
            droneService.checkDroneBatteryLevel(301L);
        } catch (CustomException e) {
            assertThat("Drone info cannot be found").isEqualTo(e.getMessage());
        }
    }

}
