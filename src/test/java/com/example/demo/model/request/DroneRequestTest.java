package com.example.demo.model.request;

import com.example.demo.core.exceptions.DroneSerialNumberException;
import com.example.demo.core.exceptions.WeightException;
import com.example.demo.model.enums.DroneModel;
import com.example.demo.model.enums.DroneState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/11/22
 * Time: 9:35 PM
 */
class DroneRequestTest {

    private DroneRequest request;

    @BeforeEach
    public void setup() {
        request = new DroneRequest();
    }

    @Test
    @DisplayName("Drone Request Pojo OK")
    void testDroneRequestPojoIsOk() {
        request = new DroneRequest(
                UUID.randomUUID().toString(),
                DroneModel.CRUISER_WEIGHT,
                100,
                100,
                DroneState.DELIVERED
        );

        assertNotNull(request);
        assertThat(100).isEqualTo(request.getWeight());
    }

    @Test
    @DisplayName("Drone Request test should fail if Serial No length is more than 100")
    void testDroneRequestSerialNoShouldFailIfMoreThan100CharacterLength() {
        request = new DroneRequest(
                UUID.randomUUID().toString() + "-" + UUID.randomUUID().toString() + "-" + UUID.randomUUID().toString() + "-" + UUID.randomUUID().toString() + "-" + UUID.randomUUID().toString() + "-" + UUID.randomUUID().toString(),
                DroneModel.CRUISER_WEIGHT,
                100,
                100,
                DroneState.DELIVERED
        );

        RuntimeException exception = assertThrows(DroneSerialNumberException.class, () -> {
            throw new DroneSerialNumberException("Drone serial number cannot be greater than 100 character long");
        });
        assertThat("Drone serial number cannot be greater than 100 character long").isEqualTo(exception.getMessage());
    }

    @Test
    @DisplayName("Drone Request Pojo Should Fail If Weight is more than 500gr")
    void testDroneRequestPojoShouldFailIfWeightIfGreaterThan500() {

        request = new DroneRequest(
                UUID.randomUUID().toString(),
                DroneModel.CRUISER_WEIGHT,
                5010,
                100,
                DroneState.DELIVERED
        );
        RuntimeException exception = assertThrows(WeightException.class, () -> {
            throw new WeightException("Drone cannot carry weight above 500gr");
        });

        assertThat("Drone cannot carry weight above 500gr").isEqualTo(exception.getMessage());
    }

    @Test
    @DisplayName("Drone Request Pojo Should Fail If Weight less than 1")
    void testDroneRequestPojoFailWeightValidation() {

        request = new DroneRequest(
                UUID.randomUUID().toString(),
                DroneModel.CRUISER_WEIGHT,
                0,
                100,
                DroneState.DELIVERED
        );
        RuntimeException exception = assertThrows(WeightException.class, () -> {
            throw new WeightException("Drone cannot carry weight less than 1");
        });
        assertThat("Drone cannot carry weight less than 1").isEqualTo(exception.getMessage());
    }

}
