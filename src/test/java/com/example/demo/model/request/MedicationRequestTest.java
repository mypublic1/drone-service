package com.example.demo.model.request;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/11/22
 * Time: 9:47 PM
 */
class MedicationRequestTest {

    private MedicationRequest request;

    @BeforeEach
    public void setup() {
        request = new MedicationRequest();
    }

    @Test
    @DisplayName("Medication Pojo Is OK")
    void testMedicationPojoIsOk() {
        request = new MedicationRequest(
                "John Doe",
                100,
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString()
        );
        assertNotNull(request);
        assertThat(100).isEqualTo(request.getWeight());
    }


    @Test
    @DisplayName("Drone Request Pojo should fail if weight is greater than 500gr")
    void testMedicationRequestPojoWeightShouldFailIfWeightIsGreaterThan500gr() {

        request = new MedicationRequest(
                "John Doe",
                100,
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString()
        );
        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            throw new RuntimeException("Medication weight cannot be greater than 500gr");
        });

        assertThat("Medication weight cannot be greater than 500gr").isEqualTo(exception.getMessage());
    }

    @Test
    @DisplayName("Medication Request should Fail if Weight is less than 1")
    void testMedicationRequestShouldFailIfWeightIsLessThan1() {

        request = new MedicationRequest(
                "John Doe",
                0,
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString()
        );
        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            throw new RuntimeException("Medication weight less than 1");
        });
        assertThat("Medication weight less than 1").isEqualTo(exception.getMessage());
    }

}
