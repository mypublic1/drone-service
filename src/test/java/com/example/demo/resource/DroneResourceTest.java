package com.example.demo.resource;

import com.example.demo.model.enums.DroneModel;
import com.example.demo.model.enums.DroneState;
import com.example.demo.model.request.DroneRequest;
import com.example.demo.model.response.AppResponse;
import com.example.demo.model.response.DroneResponse;
import com.example.demo.service.DroneService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.when;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/11/23
 * Time: 10:45 AM
 */
class DroneResourceTest {

    @Mock
    private DroneService droneService;

    @InjectMocks
    private DroneResource droneResource;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testRegisterDrone() {
        when(droneService.registerDrone(any())).thenReturn(new DroneResponse(Long.valueOf(1), "serialNo", DroneModel.LIGHT_WEIGHT, Integer.valueOf(0), "batteryCapacity", DroneState.IDLE, "dateCreated"));
        ResponseEntity<AppResponse<DroneResponse>> result = droneResource.registerDrone(new DroneRequest("serialNo", DroneModel.LIGHT_WEIGHT, Integer.valueOf(0), Integer.valueOf(0), DroneState.IDLE));
        assertNotNull(result.getBody());
        assertNotNull(result.getBody().getData());
        assertThat("serialNo").isEqualTo(result.getBody().getData().getSerialNo());
    }

    @Test
    void testLoadDroneWithMedication() {
        when(droneService.loadDroneWithMedication(anyLong(), any())).thenReturn(new DroneResponse(Long.valueOf(1), "serialNo", DroneModel.LIGHT_WEIGHT, Integer.valueOf(0), "batteryCapacity", DroneState.IDLE, "dateCreated"));
        ResponseEntity<AppResponse<DroneResponse>> result = droneResource.loadDroneWithMedication(Long.valueOf(1), Set.of(Long.valueOf(1)));
        assertNotNull(result.getBody());
        assertNotNull(result.getBody().getData());
        assertThat("serialNo").isEqualTo(result.getBody().getData().getSerialNo());
    }

    @Test
    void testCheckLoadedMedicationItemForDrone() {
        when(droneService.checkLoadedMedicationItemForDrone(anyLong())).thenReturn(new DroneResponse(Long.valueOf(1), "serialNo", DroneModel.LIGHT_WEIGHT, Integer.valueOf(0), "batteryCapacity", DroneState.IDLE, "dateCreated"));

        ResponseEntity<AppResponse<DroneResponse>> result = droneResource.checkLoadedMedicationItemForDrone(Long.valueOf(1));
        assertNotNull(result.getBody());
        assertNotNull(result.getBody().getData());
        assertThat("serialNo").isEqualTo(result.getBody().getData().getSerialNo());
    }

    @Test
    void testCheckAvailableDroneForLoading() {
        when(droneService.checkAvailableDroneForLoading(any())).thenReturn(List.of(new DroneResponse(Long.valueOf(1), "serialNo", DroneModel.LIGHT_WEIGHT, Integer.valueOf(0), "batteryCapacity", DroneState.IDLE, "dateCreated")));

        ResponseEntity<AppResponse<List<DroneResponse>>> result = droneResource.checkAvailableDroneForLoading(DroneState.IDLE);
        assertNotNull(result.getBody());
        assertNotNull(result.getBody().getData());
        assertTrue(result.getBody().getData().size() > 0);
    }

    @Test
    void testCheckDroneBatteryLevel() {
        when(droneService.checkDroneBatteryLevel(anyLong())).thenReturn(Integer.valueOf(30));
        ResponseEntity<AppResponse<Integer>> result = droneResource.checkDroneBatteryLevel(Long.valueOf(1));
        assertNotNull(result.getBody());
        assertNotNull(result.getBody().getData());
        assertThat(result.getBody().getData()).isBetween(25, 100);
    }
}
