package com.example.demo.resource;

import com.example.demo.core.constants.AppConstant;
import com.example.demo.core.utils.AppUtils;
import com.example.demo.model.enums.DroneModel;
import com.example.demo.model.enums.DroneState;
import com.example.demo.model.request.DroneRequest;
import com.example.demo.service.DroneService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/11/23
 * Time: 10:00 AM
 */
@WebMvcTest(DroneResource.class)
class DroneResourceMockMvcTest {

    @MockBean
    private DroneService droneService;

    @Autowired
    private MockMvc mockMvc;

    private DroneRequest request;

    @BeforeEach
    public void setUp() {
        request = new DroneRequest(
                "065-52-7231",
                DroneModel.CRUISER_WEIGHT,
                100,
                100,
                DroneState.DELIVERED
        );
    }

    @SneakyThrows
    @Test
    void testRegisterDrone() {
        String json = AppUtils.toJson(request);

        mockMvc.perform(post("/drones")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value(AppConstant.ResponseCode.CREATED))
                .andExpect(jsonPath("$.status").value("CREATED"));
    }

    @SneakyThrows
    @Test
    void testLoadDroneWithMedication() {
        String json = AppUtils.toJson(Set.of(10));

        mockMvc.perform(patch("/drones/load/{droneId}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value(AppConstant.ResponseCode.OK))
                .andExpect(jsonPath("$.status").value("OK"));
    }

    @SneakyThrows
    @Test
    void testCheckLoadedMedicationItemForDrone() {
        mockMvc.perform(get("/drones/{droneId}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value(AppConstant.ResponseCode.OK))
                .andExpect(jsonPath("$.status").value("OK"));
    }

    @SneakyThrows
    @Test
    void testCheckAvailableDroneForLoading() {
        mockMvc.perform(get("/drones/available")
                        .queryParam("droneState", DroneState.IDLE.name())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value(AppConstant.ResponseCode.OK))
                .andExpect(jsonPath("$.status").value("OK"));
    }

    @SneakyThrows
    @Test
    void testCheckDroneBatteryLevel() {
        mockMvc.perform(get("/drones/batteryLevel/{droneId}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value(AppConstant.ResponseCode.OK))
                .andExpect(jsonPath("$.status").value("OK"));
    }
}
