package com.example.demo.core.exceptions;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/11/22
 * Time: 11:29 PM
 */
@NoArgsConstructor
@Getter
public class WeightException extends RuntimeException {

    private String message;

    public WeightException(String message) {
        super(message);
        this.message = message;
    }
}
