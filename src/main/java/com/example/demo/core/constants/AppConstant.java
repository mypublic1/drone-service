package com.example.demo.core.constants;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/12/22
 * Time: 2:06 AM
 */
public interface AppConstant {

    public interface ResponseCode {
        public String OK = "Success";
        public String CREATED = "Success created";
        public String UNAUTHORIZED = "You are not authorized";
        public String UNKNOWN_STAFF = "Unknown staff trying to access the system. Please enter your UUID";
    }
}
