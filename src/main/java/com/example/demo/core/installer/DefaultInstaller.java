package com.example.demo.core.installer;

import com.example.demo.persistence.entity.Drone;
import com.example.demo.persistence.entity.Medication;
import com.example.demo.persistence.repository.DroneRepository;
import com.example.demo.persistence.repository.MedicationRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.List;

import static com.example.demo.core.utils.AppUtils.models;
import static com.example.demo.core.utils.AppUtils.states;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/12/22
 * Time: 12:00 AM
 */
@Service
@RequiredArgsConstructor
public class DefaultInstaller implements ApplicationListener<ContextRefreshedEvent> {

    private final Faker faker = new Faker();
    private final DroneRepository droneRepository;
    private final MedicationRepository medicationRepository;
    private boolean alreadySetup;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (alreadySetup) {
            return;
        }

        for (int i = 0; i <= 10; i++) {
            createDroneIfNotExist();
        }

        List<Drone> drones = fetchAll();

        for (int i = 0; i <= 200; i++) {
            createMedicationIfNotExist(drones.get(new SecureRandom().nextInt(drones.size())));
        }


        alreadySetup = true;
    }

    private void createMedicationIfNotExist(Drone drone) {
        long count = medicationRepository.count();
        if (count > 199) {
            return;
        }
        medicationRepository.save(Medication.builder()
                .name(faker.name().fullName())
                .code(faker.code().asin())
                .weight(faker.number().numberBetween(10, 250))
                .image(faker.avatar().image())
                .drone(drone)
                .build());
    }

    private void createDroneIfNotExist() {
        long count = droneRepository.count();
        if (count > 5) {
            return;
        }
        droneRepository.save(Drone.builder()
                .serialNo(faker.idNumber().ssnValid())
                .model(models().get(new SecureRandom().nextInt(models().size())))
                .state(states().get(new SecureRandom().nextInt(states().size())))
                .batteryCapacity(faker.number().numberBetween(20, 100))
                .weight(faker.number().numberBetween(100, 500))
                .build());
    }

    protected List<Drone> fetchAll() {
        return droneRepository.findAll();
    }
}
