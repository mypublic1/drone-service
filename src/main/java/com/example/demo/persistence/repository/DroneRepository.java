package com.example.demo.persistence.repository;

import com.example.demo.model.enums.DroneState;
import com.example.demo.persistence.entity.Drone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/11/22
 * Time: 10:28 PM
 */
public interface DroneRepository extends JpaRepository<Drone, Long> {
    Optional<Drone> findBySerialNo(String serialNo);

    List<Drone> findDroneByState(DroneState state);

    @Query("select u.batteryCapacity from Drone u where u.id = :id")
    Optional<Integer> findDroneByBatteryLevel(Long id);
}
