package com.example.demo.persistence.repository;

import com.example.demo.persistence.entity.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/11/22
 * Time: 10:53 PM
 */
public interface MedicationRepository extends JpaRepository<Medication, Long> {
}
