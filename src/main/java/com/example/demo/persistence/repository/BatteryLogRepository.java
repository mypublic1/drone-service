package com.example.demo.persistence.repository;

import com.example.demo.persistence.entity.BatteryLog;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/12/22
 * Time: 3:42 AM
 */
public interface BatteryLogRepository extends JpaRepository<BatteryLog, Long> {
}
