package com.example.demo.persistence.entity;

import com.example.demo.model.enums.DroneModel;
import com.example.demo.model.enums.DroneState;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/11/22
 * Time: 10:23 PM
 */
@Entity
@Table(name = "drones")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Drone implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "serial_no")
    private String serialNo;

    @Column(name = "model")
    @Enumerated(EnumType.STRING)
    private DroneModel model;

    @Column(name = "weight")
    private Integer weight;

    @Column(name = "battary_capacity")
    private Integer batteryCapacity;

    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private DroneState state;

    @Column(name = "date_created")
    private Timestamp dateCreated;

    @OneToMany(mappedBy = "drone")
    @JsonIgnore
    private Set<Medication> medications;

    public Drone(String serialNo, Integer weight, DroneModel model, DroneState state) {
        this.serialNo = serialNo;
        this.weight = weight;
        this.model = model;
        this.state = state;
    }

    @PrePersist
    protected void onCreate() {
        this.dateCreated = new Timestamp(System.currentTimeMillis());
    }
}
