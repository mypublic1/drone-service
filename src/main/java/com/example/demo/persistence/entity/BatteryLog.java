package com.example.demo.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/12/22
 * Time: 3:38 AM
 */
@Entity
@Table(name = "battery_logs")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class BatteryLog implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "change_log", length = 5_000, updatable = false)
    private String changeLog;

    @Column(name = "drone_fk", updatable = false)
    private Long droneId;

    @Column(name = "time_changed", updatable = false)
    private Timestamp timeChanged;
}
