package com.example.demo.resource;

import com.example.demo.core.constants.AppConstant;
import com.example.demo.model.enums.DroneState;
import com.example.demo.model.request.DroneRequest;
import com.example.demo.model.response.AppResponse;
import com.example.demo.model.response.DroneResponse;
import com.example.demo.service.DroneService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/12/22
 * Time: 2:01 AM
 */
@RestController
@RequestMapping("drones")
@RequiredArgsConstructor
public class DroneResource {

    private final DroneService droneService;

    @PostMapping
    public ResponseEntity<AppResponse<DroneResponse>> registerDrone(@RequestBody @Valid DroneRequest request) {
        DroneResponse response = droneService.registerDrone(request);
        return ResponseEntity.ok().body(AppResponse.<DroneResponse>builder()
                .data(response)
                .status(HttpStatus.CREATED)
                .message(AppConstant.ResponseCode.CREATED)
                .build());
    }

    @PatchMapping("load/{droneId}")
    public ResponseEntity<AppResponse<DroneResponse>> loadDroneWithMedication(@PathVariable Long droneId, @RequestBody Set<Long> medications) {
        DroneResponse response = droneService.loadDroneWithMedication(droneId, medications);
        return ResponseEntity.ok().body(AppResponse.<DroneResponse>builder()
                .data(response)
                .status(HttpStatus.OK)
                .message(AppConstant.ResponseCode.OK)
                .build());
    }

    @GetMapping("{droneId}")
    public ResponseEntity<AppResponse<DroneResponse>> checkLoadedMedicationItemForDrone(@PathVariable Long droneId) {
        DroneResponse response = droneService.checkLoadedMedicationItemForDrone(droneId);
        return ResponseEntity.ok().body(AppResponse.<DroneResponse>builder()
                .data(response)
                .status(HttpStatus.OK)
                .message(AppConstant.ResponseCode.OK)
                .build());
    }

    @GetMapping("available")
    public ResponseEntity<AppResponse<List<DroneResponse>>> checkAvailableDroneForLoading(DroneState droneState) {
        List<DroneResponse> response = droneService.checkAvailableDroneForLoading(droneState);
        return ResponseEntity.ok().body(AppResponse.<List<DroneResponse>>builder()
                .data(response)
                .status(HttpStatus.OK)
                .message(AppConstant.ResponseCode.OK)
                .build());
    }


    @GetMapping("batteryLevel/{droneId}")
    public ResponseEntity<AppResponse<Integer>> checkDroneBatteryLevel(@PathVariable Long droneId) {
        Integer response = droneService.checkDroneBatteryLevel(droneId);
        return ResponseEntity.ok().body(AppResponse.<Integer>builder()
                .data(response)
                .status(HttpStatus.OK)
                .message(AppConstant.ResponseCode.OK)
                .build());
    }

}
