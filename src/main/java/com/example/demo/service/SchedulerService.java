package com.example.demo.service;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/12/22
 * Time: 3:14 AM
 */
public interface SchedulerService {

    void changeDroneState();

    void updateDroneBattery();

}
