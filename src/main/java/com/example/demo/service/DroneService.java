package com.example.demo.service;

import com.example.demo.model.enums.DroneState;
import com.example.demo.model.request.DroneRequest;
import com.example.demo.model.response.DroneResponse;

import java.util.List;
import java.util.Set;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/11/22
 * Time: 11:05 PM
 */
public interface DroneService {

    DroneResponse registerDrone(DroneRequest request);

    DroneResponse loadDroneWithMedication(Long droneId, Set<Long> medicationIds);

    DroneResponse checkLoadedMedicationItemForDrone(Long droneId);

    List<DroneResponse> checkAvailableDroneForLoading(DroneState state);

    Integer checkDroneBatteryLevel(Long droneId);
}
