package com.example.demo.service.impl;

import com.example.demo.core.utils.AppUtils;
import com.example.demo.persistence.entity.BatteryLog;
import com.example.demo.persistence.entity.Drone;
import com.example.demo.persistence.repository.BatteryLogRepository;
import com.example.demo.persistence.repository.DroneRepository;
import com.example.demo.service.SchedulerService;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.List;

import static com.example.demo.core.utils.AppUtils.states;
import static com.example.demo.core.utils.AppUtils.weakStates;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/12/22
 * Time: 3:15 AM
 */
@Service
@RequiredArgsConstructor
public class SchedulerServiceImpl implements SchedulerService {

    private final DroneRepository droneRepository;
    private final BatteryLogRepository batteryLogRepository;

    /**
     * the cron job is configured to run at every one minute
     */
    @Override
    @Scheduled(cron = "0 0/1 * 1/1 * *")
    public void changeDroneState() {
        List<Drone> drones = droneRepository.findAll();

        if (drones.isEmpty()) {
            return;
        }

        drones.forEach(drone -> {
            if (drone.getBatteryCapacity() <= 25) {
                drone.setState(states().get(new SecureRandom().nextInt(weakStates().size())));
            } else {
                drone.setState(states().get(new SecureRandom().nextInt(states().size())));
            }
            droneRepository.save(drone);
        });
    }

    /**
     * the cron job is configured to run at every two minute
     */
    @Override
    @Scheduled(cron = "0 0/2 * 1/1 * *")
    public void updateDroneBattery() {
        Faker f = new Faker();
        List<Drone> drones = droneRepository.findAll();

        if (drones.isEmpty()) {
            return;
        }

        drones.forEach(drone -> {
            batteryLogRepository.save(BatteryLog.builder()
                    .droneId(drone.getId())
                    .changeLog(AppUtils.toJson(drone))
                    .timeChanged(new Timestamp(System.currentTimeMillis()))
                    .build());
        });

        drones.forEach(drone -> {
            drone.setBatteryCapacity(f.number().numberBetween(25, 100));
            droneRepository.save(drone);

            batteryLogRepository.save(BatteryLog.builder()
                    .droneId(drone.getId())
                    .changeLog(AppUtils.toJson(drone))
                    .timeChanged(new Timestamp(System.currentTimeMillis()))
                    .build());

        });

    }
}
