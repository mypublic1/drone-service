package com.example.demo.service.impl;

import com.example.demo.core.exceptions.CustomException;
import com.example.demo.core.exceptions.WeightException;
import com.example.demo.core.utils.ModelMapperUtils;
import com.example.demo.model.enums.DroneState;
import com.example.demo.model.request.DroneRequest;
import com.example.demo.model.response.DroneResponse;
import com.example.demo.persistence.entity.Drone;
import com.example.demo.persistence.entity.Medication;
import com.example.demo.persistence.repository.DroneRepository;
import com.example.demo.persistence.repository.MedicationRepository;
import com.example.demo.service.DroneService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/11/22
 * Time: 11:06 PM
 */
@Service
@RequiredArgsConstructor
public class DroneServiceImpl implements DroneService {

    private final DroneRepository droneRepository;
    private final MedicationRepository medicationRepository;

    @Override
    public DroneResponse registerDrone(DroneRequest request) {
        droneRepository.findBySerialNo(request.getSerialNo()).ifPresent(data -> {
            throw new CustomException("Drone with serial number [" + request.getSerialNo() + "] already exist", HttpStatus.PRECONDITION_FAILED);
        });
        return ModelMapperUtils.map(droneRepository.save(Drone.builder()
                .serialNo(request.getSerialNo())
                .model(request.getModel())
                .weight(request.getWeight())
                .batteryCapacity(request.getBatteryCapacity())
                .state(request.getState())
                .build()), DroneResponse.class);
    }

    @Override
    public DroneResponse loadDroneWithMedication(Long droneId, Set<Long> medicationIds) {
        Drone drone = droneRepository.findById(droneId).orElseThrow(() -> new CustomException("Drone cannot be found", HttpStatus.NOT_FOUND));

        if (drone.getBatteryCapacity() <= 25) {
            throw new CustomException("The drone cannot be loaded due to battery level", HttpStatus.PRECONDITION_FAILED);
        }
        List<Medication> medications = medicationRepository.findAllById(medicationIds);
        if (medications.isEmpty()) {
            throw new CustomException("Medication records cannot be found", HttpStatus.NOT_FOUND);
        }

        Integer reduce = medications.stream().map(Medication::getWeight).reduce(0, Integer::sum);
        if (reduce >= drone.getWeight()) {
            throw new WeightException("Medication weight is higher than the drone weight");
        }
        medications.forEach(medication -> {
            medication.setDrone(drone);
            medicationRepository.save(medication);
        });
        return ModelMapperUtils.map(drone, DroneResponse.class);
    }

    @Override
    public DroneResponse checkLoadedMedicationItemForDrone(Long droneId) {

        Drone drone = droneRepository.findById(droneId).orElseThrow(() -> new CustomException("Drone info cannot be found", HttpStatus.NOT_FOUND));

        return ModelMapperUtils.map(drone, DroneResponse.class);
    }

    @Override
    public List<DroneResponse> checkAvailableDroneForLoading(DroneState state) {
        List<Drone> drones = droneRepository.findDroneByState(state);
        if (drones.isEmpty()) {
            throw new CustomException("No record found", HttpStatus.NOT_FOUND);
        }
        return ModelMapperUtils.mapAll(drones.stream().map(drone ->
                        drone.getState().equals(DroneState.IDLE) && drone.getBatteryCapacity() > 25)
                .collect(Collectors.toList()), DroneResponse.class);
    }

    @Override
    public Integer checkDroneBatteryLevel(Long droneId) {
        return droneRepository.findDroneByBatteryLevel(droneId).orElseThrow(() -> new CustomException("Drone info cannot be found", HttpStatus.NOT_FOUND));
    }
}
