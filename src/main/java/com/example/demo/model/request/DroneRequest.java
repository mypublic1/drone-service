package com.example.demo.model.request;

import com.example.demo.core.exceptions.DroneSerialNumberException;
import com.example.demo.core.exceptions.WeightException;
import com.example.demo.model.enums.DroneModel;
import com.example.demo.model.enums.DroneState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/11/22
 * Time: 9:36 PM
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class DroneRequest implements Serializable {

    @NotBlank(message = "Serial number cannot be blank")
    @NotNull(message = "Serial number cannot be empty")
    private String serialNo;
    @NotNull(message = "Drone model cannot be empty")
    private DroneModel model;
    @NotNull(message = "Drone weight cannot be empty")
    private Integer weight;
    @NotNull(message = "Drone battery capacity cannot be empty")
    private Integer batteryCapacity;
    @NotNull(message = "Drone state cannot be empty")
    private DroneState state;

    public String getSerialNo() {
        if (this.serialNo.length() > 100) {
            throw new DroneSerialNumberException("Drone serial number cannot be greater than 100 character long");
        }
        return serialNo;
    }

    public Integer getWeight() {
        if (this.weight < 1) {
            throw new WeightException("Drone cannot carry weight less than 1");
        }
        if (this.weight > 500) {
            throw new WeightException("Drone cannot carry weight above 500gr");
        }
        return weight;
    }


}
