package com.example.demo.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/11/22
 * Time: 9:48 PM
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class MedicationRequest implements Serializable {

    private String name;
    private Integer weight;
    private String code;
    private String image;

    public Integer getWeight() {
        if (this.weight < 1) {
            throw new RuntimeException("Medication weight less than 1");
        }
        if (this.weight > 500) {
            throw new RuntimeException("Medication weight cannot be greater than 500gr");
        }
        return weight;
    }
}
