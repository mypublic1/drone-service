package com.example.demo.model.response;

import com.example.demo.model.enums.DroneModel;
import com.example.demo.model.enums.DroneState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/11/22
 * Time: 11:01 PM
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class DroneResponse implements Serializable {

    private Long id;
    private String serialNo;
    private DroneModel model;
    private Integer weight;
    private String batteryCapacity;
    private DroneState state;
    private String dateCreated;

    public String getBatteryCapacity() {
        return batteryCapacity + "%";
    }
}
