package com.example.demo.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/11/22
 * Time: 9:41 PM
 */
@Getter
@AllArgsConstructor
public enum DroneState {

    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}
