package com.example.demo.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Project title: demo
 *
 * @author johnadeshola
 * Date: 6/11/22
 * Time: 9:37 PM
 */
@Getter
@AllArgsConstructor
public enum DroneModel {

    LIGHT_WEIGHT("Lightweight"),
    MIDDLE_WEIGHT("Middleweight"),
    CRUISER_WEIGHT("Cruiserweight"),
    HEAVY_WEIGHT("Heavyweight");

    private final String model;
}
