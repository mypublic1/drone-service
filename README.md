[[DRONE RESTFUL SERVICE]]

**Tech Stack**

* Java 11
* Spring boot
* Hibernate framework
* H2 in-memory database
* Spring data JPA
* Swagger Documentation
* Spring validation
* Maven

**Steps on how to execute the application**

* The application make use of In-memory Database, H2 with name: **dronedb**
* The database username and password are (sa/sa)
* run **mvn clean install** or **mvn clean install -DskipTests** should in case some test cases failed.
* execute from the application base java -jar target/drone-service.jar or hit the run button from the IDE
* [Swagger URL](http://localhost:8081/api/v1/swagger-ui/index.html)
* [Postman Collection](https://documenter.getpostman.com/view/847081/Uz5NjDVE)
* Data are automatically populated to the database when the application start

**Drone Battery Log**

* Drone battery audit/history logs are save in the battery_logs table. It stores every logs when the drone battery is
  updated at interval.
